# Web3Auth & OspClient Quick Start

- 此demo使用web3Auth提供的EOA钱包。
- 使用EOA钱包登录OspClient，于OSP交互。
- OspClient登录后会为用户生成一个AA钱包。
- 创建Profile上链需要Gas，使用SoMon提供的Gas补贴服务。

## 准备工作
```bash
npm install
```

## 运行
```bash
npm run dev
```
