'use client';
import {useEffect, useState} from "react";
import {Web3Auth} from "@web3auth/modal";
import {CHAIN_NAMESPACES, IProvider, WEB3AUTH_NETWORK} from "@web3auth/base";

import "./App.css";
import {EthereumPrivateKeyProvider} from "@web3auth/ethereum-provider";
import {ethers} from "ethers";
import {ENV, OSP_APP_ID} from "../config";
import {getGasPolicy} from "../policy";
import {OspClient, ProfileError, ProfileErrorCodeEnum} from "@open-social-protocol/osp-client";
import {Middleware, OperationOverrides, UserOperation} from "@open-social-protocol/osp-wallet";

const clientId = "BCMEMo_PR8MTJolWeMOG8Ae5gdzvKhuxk9FVAwCP_ErU1BOEk21SYNPwDAwZQR1Veg4NYeMBhFBrtGjVKDUag3A"; // get from https://dashboard.web3auth.io

const chainConfig = {
    chainId: "0x14a34", // Please use 0x1 for Mainnet
    rpcTarget: "https://sepolia.base.org",
    chainNamespace: CHAIN_NAMESPACES.EIP155,
    displayName: "Base Sepolia",
    blockExplorerUrl: "https://sepolia-explorer.base.org",
    ticker: "ETH",
    tickerName: "Ethereum",
    logo: "https://images.toruswallet.io/eth.svg",
};

const privateKeyProvider = new EthereumPrivateKeyProvider({
    config: {chainConfig: chainConfig},
});

const web3auth = new Web3Auth({
    clientId,
    web3AuthNetwork: WEB3AUTH_NETWORK.SAPPHIRE_DEVNET,
    privateKeyProvider: privateKeyProvider,
});

function App() {
    const [provider, setProvider] = useState<IProvider | null>(null);
    const [loggedIn, setLoggedIn] = useState(false);
    const [etherProvider, setEthProvider] = useState<ethers.providers.Web3Provider | null>(null);
    const [ospClient, setOspClient] = useState<OspClient | null>(null);
    const [handle, setHandle] = useState('handle_name');

    useEffect(() => {
        const init = async () => {
            try {
                const ospClient = await OspClient.create({
                    env: ENV,
                    app_id: OSP_APP_ID,
                    storage: localStorage,
                    // guest_id_marketing：一个随机数，用来标记未登录的用户身份。如果用户登录前后该值保持一致，那么该用户在登陆之前的喜好将会被记录。
                    guest_id_marketing: '127461275467125462'
                });
                const GasSubsidyMiddleware: Middleware = async (userOperation: UserOperation, opt: OperationOverrides): Promise<[UserOperation, OperationOverrides]> => {
                    const gasPolicy = await getGasPolicy(userOperation, (await ospClient.authentication.idToken()).id_token);
                    console.log(gasPolicy);
                    opt.policy_data = gasPolicy;
                    return [userOperation, opt];
                }
                ospClient.txClient.addMiddleware(GasSubsidyMiddleware, 150);
                setOspClient(ospClient)
                await web3auth.initModal();
                setProvider(web3auth.provider);
                const etherProvider = new ethers.providers.Web3Provider(web3auth.provider);
                setEthProvider(etherProvider);


                if (web3auth.connected) {
                    setLoggedIn(true);
                    // 每次初始化ospClient实例之后都需要调用login方法。目的是把signer传入SDK
                    await ospClient.authentication.login(etherProvider.getSigner(), {'Os-Web3-Auth-Id-Token': (await web3auth.authenticateUser()).idToken})
                }
            } catch (error) {
                console.error(error);
            }
        };

        init();
    }, []);

    const login = async () => {
        const web3authProvider = await web3auth.connect();
        setProvider(web3authProvider);
        if (web3auth.connected) {
            setLoggedIn(true);
            // 只有使用Web3Auth的社交登录的用户才能享受SoMon的Gas补贴。
            // 首次登陆时必须传入Web3Auth的IdToken，否则后端无法判断该用户是否是Web3Auth的社交登录，导致没有Gas补贴。
            await ospClient.authentication.login(etherProvider.getSigner(), {'Os-Web3-Auth-Id-Token': (await web3auth.authenticateUser()).idToken})
        }
    };

    const getUserInfo = async () => {
        const user = await web3auth.getUserInfo();
        uiConsole(user);
    };

    const logout = async () => {
        await web3auth.logout();
        await ospClient.authentication.logout();
        setProvider(null);
        setLoggedIn(false);
        uiConsole("logged out");
    };

    const getEoaAddress = async () => {
        if (!provider) {
            uiConsole("provider not initialized yet");
            return;
        }
        // Get user's Ethereum public address
        const address = await etherProvider.getSigner().getAddress();
        uiConsole(address.toLowerCase());
    };

    const getWeb3AuthIdToken = async () => {
        if (!provider) {
            uiConsole("provider not initialized yet");
            return;
        }
        uiConsole(await web3auth.authenticateUser());
    };

    const getOspAddress = async () => {
        // 获取当前登录用户的AA钱包地址。
        const address = await ospClient.txClient.getAccountAddress();
        uiConsole(`ospAddress:${address.toLowerCase()}`);
    };

    const getOwnerProfile = async () => {
        // 获取当前登录用户的Profile信息，如果不存在则返回null。
        const profile = await ospClient.profile.get();
        uiConsole('profile:', profile);
    };

    const createProfile = async () => {
        const profileHandle = new RegExp('^[0-9a-z_]{4,15}$').exec(handle)?.at(0);
        // handle 由 4 - 15 位数字、字母、下划线组成。
        if (profileHandle == null) {
            uiConsole("handle name is invalid");
            return;
        }
        if ((await ospClient.profile.getByHandle(profileHandle)) != null) {
            // 在后端查询handle是否占用，因为内部环境有许多脏数据，这一步并不能完全检测出handle重复。在下面上链时还会出现一个handle重复的错误码。
            uiConsole("handle name already exists");
            return;
        }
        try {
            // profile是一种SBT，会被mint到用户的AA钱包上。
            const profile = await ospClient.profile.create({
                handle: handle
            });
            uiConsole('create profile:', profile);
        } catch (e) {
            if (e instanceof ProfileError && e.code == ProfileErrorCodeEnum.HANDLE_TAKEN) {
                // 上链时检测到handle重复。
                uiConsole("handle name already exists");
            } else {
                uiConsole('create profile error:', e);
            }
        }
    };


    function uiConsole(...args: any[]): void {
        const el = document.querySelector("#console>p");
        if (el) {
            el.innerHTML = JSON.stringify(args || {}, null, 2);
        }
        console.log(...args);
    }

    const loggedInView = (
        <>
            <p>web3Auth</p>
            <div className="flex-container">
                <div>
                    <button onClick={getUserInfo} className="card">
                        Get User Info
                    </button>
                </div>
                <div>
                    <button onClick={getEoaAddress} className="card">
                        Get Eoa Address
                    </button>
                </div>
                <div>
                    <button onClick={getWeb3AuthIdToken} className="card">
                        Web3Auth IdToken
                    </button>
                </div>
                <div>
                    <button onClick={logout} className="card">
                        Log Out
                    </button>
                </div>
            </div>
            <p>OSP SDK 查询</p>

            <div className="flex-container">
                <div>
                    {/*获取AA钱包地址*/}
                    <button onClick={getOspAddress} className="card">
                        Get OSP Address
                    </button>
                </div>
                <div>
                    {/*查询AA钱包的Profile*/}
                    <button onClick={getOwnerProfile} className="card">
                        Get Profile
                    </button>
                </div>
            </div>
            <p>创建Profile</p>
            <label>
                handle name:
                <input className="input" name="handleInput" value={handle} onChange={e => setHandle(e.target.value)}/>
            </label>
            <div>
                <button onClick={createProfile} className="card">
                    Create Profile
                </button>
            </div>
        </>
    );

    const unloggedInView = (
        <button onClick={login} className="card">
            Login
        </button>
    );

    return (
        <div className="container">
            <h1 className="title">
                <a target="_blank" href="https://web3auth.io/docs/sdk/pnp/web/modal" rel="noreferrer">
                    Web3Auth{" "}
                </a>
                & <a target="_blank" href="https://www.npmjs.com/package/@open-social-protocol/osp-client"
                     rel="noreferrer">
                OspClient{" "}
            </a> Quick Start
            </h1>
            <div className="grid">{loggedIn ? loggedInView : unloggedInView}</div>
            <div id="console" style={{whiteSpace: "pre-line"}}>
                <p style={{whiteSpace: "pre-line"}}></p>
            </div>
        </div>
    );
}

export default App;
