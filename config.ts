import {Environment} from "@open-social-protocol/osp-client";

export const ENV = Environment.dev;

const PolicyServerUrlMao: Partial<Record<Environment, string>> = {
    dev: 'https://nftman-osp-policy-server-git-ospdev-loa.vercel.app/api',
    beta: 'https://nftman-osp-policy-server-git-ospbeta-loa.vercel.app/api',
    prod: 'https://gas-policy.mugenos.io/api',
};
const OspAppIdMap: Partial<Record<Environment, string>> = {
    dev: '64443516947792390',
    beta: '64446220021530632',
    prod: '64447188599570692',
};

export const POLICY_SERVER_URL = PolicyServerUrlMao[ENV];
export const OSP_APP_ID = OspAppIdMap[ENV];
