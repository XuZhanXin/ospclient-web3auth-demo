import {POLICY_SERVER_URL} from "../config";
import {UserOperation} from "@open-social-protocol/osp-wallet";
import {ethers} from "ethers";

export async function getGasPolicy(userOperation: UserOperation, ospIdToken: string) {
    const response = await getResponse(await fetch(`${POLICY_SERVER_URL}/gas/policy`, {
        headers: {
            "Content-Type": "application/json",
        },
        mode: "cors",
        method: "post",
        body: JSON.stringify({
            "sender": userOperation.sender,
            "nonce": ethers.utils.hexValue(userOperation.nonce),
            "init_code": ethers.utils.hexlify(userOperation.initCode),
            "call_data": ethers.utils.hexlify(userOperation.callData),
            "call_gas_limit": ethers.utils.hexValue(userOperation.callGasLimit),
            "verification_gas_limit": ethers.utils.hexValue(userOperation.verificationGasLimit),
            "pre_verification_gas": ethers.utils.hexValue(userOperation.preVerificationGas),
            "max_fee_per_gas": ethers.utils.hexValue(userOperation.maxFeePerGas),
            "max_priority_fee_per_gas": ethers.utils.hexValue(userOperation.maxPriorityFeePerGas),
            "paymaster_and_data": ethers.utils.hexlify(userOperation.paymasterAndData),
            "osp_id_token": ospIdToken
        })
    }));
    console.log('get gas policy res :', response);
    return response;
}

async function getResponse(response: Response) {
    if (response.status != 200) {
        throw new Error(`Failed to fetch ${response.url}: ${response.status} ${response.statusText}`);
    }
    const json = await response.json();
    if (!json.success) {
        throw new Error(json);
    }
    return json.data;
}
